import os
import json
from oslo_log import log as logging
from webob import exc

import json
from celebrer.common import wsgi
from celebrer.common import rpc
from celebrer.db import models
from celebrer.db import session

from oslo_config import cfg

CONF = cfg.CONF
LOG = logging.getLogger(__name__)


class Controller(object):

    def __init__(self):
        self.conf = CONF

    def list_services(self, request):
        unit = session.get_session()
        raw_services = unit.query(models.Service).all()
        services = []
        for service in raw_services:
            services.append(
                    {'id': service.id,
                     'name': service.name,
                     'component': service.component,
                     'status': service.status,
                     'node': unit.query(models.Node).filter_by(id=service.node_id).first().node_id
                     }
                )
        data_resp = json.dumps({"services": services})
        return data_resp

    def list_components(self, request):
        unit = session.get_session()
        components = []
        raw_services = unit.query(models.Service).all()
        for current_service in raw_services:
            if {"id": current_service.component} not in components:
                components.append({"id": current_service.component})
        data_resp = json.dumps({"components": components})
        return data_resp

    def get_agents_list(self, request):
        unit = session.get_session()
        agents = []
        raw_agents = unit.query(models.Node).all()
        for agent in raw_agents:
            services = []
            for service in agent.services:
                services.append(service.name)
            agents.append({'id': agent.node_id, 'services': services})
        data_resp = json.dumps({"agents": agents})
        return data_resp

    def run_component(self, request):
        tasks = []
        services = []
        unit = session.get_session()
        raw_services = unit.query(models.Service).filter_by(component=request.json_body['component']).all()
        
        for current_service in raw_services:
            if current_service.name not in services:
                services.append(current_service.name)

        if request.json_body['action'] == 'start' and len(services)>0:
            tasks.append(rpc.call(
                'tasks',
                'create_task',
                service_list=services))
            data_resp = json.dumps({"tasks": tasks})
        else:
            data_resp = json.dumps({"tasks": []})
        return data_resp

    def run_services(self, request):
        tasks = []

        if request.json_body['action'] == 'start':
            tasks.append(rpc.call(
                'tasks',
                'create_task',
                service_list=request.json_body['services']))
        elif request.json_body['action'] == 'stop':
            tasks.append(rpc.cast(
                'tasks',
                'stop_task',
                task_id=request.json_body['task_id']))

        data_resp = json.dumps({"tasks": tasks})
        return data_resp

    def get_results(self, request, task_id=None):
        unit = session.get_session()
        history_reports = []

        if task_id:
            task = unit.query(models.Task).filter_by(id=task_id).first()
            return json.dumps(task.to_dict())
        history = [node.to_dict() for node in
                           unit.query(models.Task).all()]

        history_reports.append(history)
        data_resp = json.dumps({"history": history})
        return data_resp

    def get_report(self, request, task_id=None):
        if not task_id:
            raise exc.HTTPNotFound('Not found task')
        if os.path.isfile('%s/%s' % (self.conf.reports_dir, task_id)):
            return json.dumps({"file": open('%s/%s' % (self.conf.reports_dir, task_id)).read().encode('base64')})
        else:
            raise exc.HTTPNotFound('Not found report')


def create_resource():
    return wsgi.Resource(Controller())
