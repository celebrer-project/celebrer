import os
import sys

from oslo_config import cfg

from celebrer.common import config
from celebrer.db import api as db_api

# If ../imagination/__init__.py exists, add ../ to Python search path, so that
# it will override what happens to be installed in /usr/(local/)lib/python...
root = os.path.join(os.path.abspath(__file__), os.pardir, os.pardir, os.pardir)
if os.path.exists(os.path.join(root, 'celebrer', '__init__.py')):
    sys.path.insert(0, root)

CONF = cfg.CONF


class DBCommand(object):

    def setup(self, config):
        db_api.setup_db()
        print "Database schema created"

    def drop(self, config):
        db_api.drop_db()
        print "Database was dropped"

    def clean(self, config):
        self.drop(config)
        self.setup(config)
        print "Database is clean"


def add_command_parsers(subparsers):
    command_object = DBCommand()

    parser = subparsers.add_parser('setup')
    parser.set_defaults(func=command_object.setup)

    parser = subparsers.add_parser('drop')
    parser.set_defaults(func=command_object.drop)

    parser = subparsers.add_parser('clean')
    parser.set_defaults(func=command_object.clean)


command_opt = cfg.SubCommandOpt('command',
                                title='Command',
                                help='Available commands',
                                handler=add_command_parsers)
CONF.register_cli_opt(command_opt)


def main():
    config.parse_args()
    CONF.command.func(config)

if __name__ == '__main__':
    main()

